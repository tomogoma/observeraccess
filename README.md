# ObserverAccess #

Demonstrate implementation of a database-agnostic interface to access different database connections in a uniform manner - by using a simplified Observer pattern.

## Implementations: ##

* **FileAccessor** - simulates a file data store that stores one line to a file
* **MemAccessor** - simulates a data store that stores data in memory

* **Access** - interface implemented by Accessors above to unify access to either type of data store

* **Main** - Demonstrates access to both Accessors in a unified way

Comments inline describe the challenges and possible workarounds