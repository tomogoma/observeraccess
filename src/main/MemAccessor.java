package main;

public class MemAccessor implements Access {
	
	private String accessText = "Memory accessor: ";

	@Override
	public void set(String set) {
		accessText += set;
	}

	@Override
	public String get() {
		return accessText;
	}

}
