/**
 * @author ogoma
 * <p>
 * Demonstrates implementing data-access-agnostic interface, {@link Access},
 * which defines methods that a file accessor class {@link FileAccessor}
 * and a memory accessor class {@link MemAccessor} both use (and any other accessor class).
 * This allows the calling class {@link Main} to access both in the same way
 * as though underlying implementation is the same
 * </p>
 */
package main;