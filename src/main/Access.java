package main;

import java.io.IOException;

public interface Access {
	
	public void set(String set) throws IOException;
	
	public String get() throws IOException;
	
	//  Disadvantage: All implementers have to define the methods listed
	//  even if they do not have such functionality
	//  Workarounds would be to:
	//  1. Use a different interface for the non-uniform functionality (Strategy Design Pattern)
	//  or
	//  2. Throw some standardised Exception e.g. AccessUnavailableException
	//     for functions that an implementer is incapable of implementing
	//  -- Strategy Pattern is preferred

}
