package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileAccessor implements Access {
	
	private String accessText = "File accessor: ";
	private String fileLocation;
	private BufferedWriter writer;
	private BufferedReader reader;
	
	public FileAccessor(String fileLocation) throws IOException {
		this.fileLocation = fileLocation;
	}

	@Override
	public void set(String set) throws IOException {
		writer = new BufferedWriter(new FileWriter(fileLocation));
		writer.write(set);
		writer.close();
	}

	@Override
	public String get() throws IOException {
		reader = new BufferedReader(new FileReader(fileLocation));
		String fileData = reader.readLine();
		reader.close();
		return accessText + fileData;
	}

}
