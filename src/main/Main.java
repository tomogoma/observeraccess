package main;

import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		
		Access fileAccessor = null; // Initialising to null in case try catch fails to initialise
		Access memoryAccessor;
		
		try {
			fileAccessor = new FileAccessor("testfile.txt");
			memoryAccessor = new MemAccessor();
			
			// Write data to both accessors
			
			if (fileAccessor != null) {
				fileAccessor.set("File Data");
			}
			
			// Disadvantage: All objects have check for exceptions of the interface; even the ones
			// that their instance classes do not throw
			// e.g. 'MemAccessor' class does not throw IOException for 'set()' method but we
			// have to check for it since 'Access' interface requires it be thrown
			// (because 'FileAccessor' class needed to throw it)
			memoryAccessor.set("Memory Data");

			//  Get data from both accessors
			
			System.out.println(fileAccessor.get());
			System.out.println(memoryAccessor.get());
			
		} catch (IOException e) {
			System.out.println("Error: " + e.getLocalizedMessage());
		}
	}

}
